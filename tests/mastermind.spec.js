const assert = require("assert");

function evaluate(secret, proposition) {
    [nbPionBienPlaces, nbPionMalPlaces] = proposition.reduce((accumulator, currentValue, currentIndex) => {
        const isPionBienPlace = currentValue == secret[currentIndex];
        const isPionMalPlace = !isPionBienPlace && secret.includes(currentValue);
        accumulator[0] += isPionBienPlace ? 1 : 0
        accumulator[1] += isPionMalPlace ? 1 : 0
        return accumulator
    }, [0, 0]);
    return {nbPionBienPlaces, nbPionMalPlaces}
}

describe("Le gardien du secret au mastermind", () => {
    [
        {secret: ["bleu"], proposition: ["rouge"], expectedEvaluation: {nbPionBienPlaces: 0, nbPionMalPlaces: 0}},
        {secret: ["bleu"], proposition: ["bleu"], expectedEvaluation: {nbPionBienPlaces: 1, nbPionMalPlaces: 0}},
        {secret: ["rouge"], proposition: ["rouge"], expectedEvaluation: {nbPionBienPlaces: 1, nbPionMalPlaces: 0}},
        {
            secret: ["rouge", "bleu"],
            proposition: ["jaune", "rouge"],
            expectedEvaluation: {nbPionBienPlaces: 0, nbPionMalPlaces: 1}
        },
        {
            secret: ["jaune", "rouge", "bleu"],
            proposition: ["jaune", "vert", "rouge"],
            expectedEvaluation: {nbPionBienPlaces: 1, nbPionMalPlaces: 1}
        },
        {
            secret: ["orange", "noir", "blanc", "jaune", "rouge", "bleu"],
            proposition: ["orange", "noir", "jaune", "blanc", "vert", "rouge"],
            expectedEvaluation: {nbPionBienPlaces: 2, nbPionMalPlaces: 3}
        },
    ].forEach(inputs => {

        const {secret, proposition, expectedEvaluation} = inputs

        it(`doit répondre ${expectedEvaluation.nbPionBienPlaces} pion(s) bien placé(s) et ${expectedEvaluation.nbPionMalPlaces} pion(s) mal placé(s)`, () => {
            const {nbPionBienPlaces, nbPionMalPlaces} = evaluate(secret, proposition)

            assert.equal(nbPionBienPlaces, expectedEvaluation.nbPionBienPlaces)
            assert.equal(nbPionMalPlaces, expectedEvaluation.nbPionMalPlaces)
        })
    })
});